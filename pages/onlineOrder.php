<?php
include_once 'components/Article.php';

$article = new Article();

$pizzasInfo = $article->getPizzaInfo();

$extras = $article->getAllChoosableExtras();

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.css">
    <title>Online Order</title>
</head>
<body>
<div class="d-flex flex-row flex-wrap justify-content-between ">
    <?php foreach ($pizzasInfo as $pizza): ?>
        <div class="col-3 mb-5 border border-secondary rounded mr-1">
            <img class="card-img-top" src="assets/img/pizza.png" alt="pizza">
            <h5><?= $pizza['name'] ?></h5>
            <p class="card-text"><?php foreach ($pizza['extras'] as $extra): ?>
                    <?= $extra['name'] ?>
                <?php endforeach; ?></p>
            <div class="d-flex flex-row justify-content-between ">
                <?php if ($pizza['ID'] == $_SESSION['sale']): ?>
                    <h5 style="color: #0d9c0d">Price: <?= round(($pizza['price']) * 0.67, 1) ?> &#8364! Sale</h5>
                <?php else: ?>
                    <h6>Price: <?= $pizza['price'] ?> &#8364</h6>
                <?php endif; ?>
                <button class="mb-2 btn btn-primary btn-sm border border-primary rounded" data-toggle="modal"
                        data-target="#exampleModalCenter<?= $pizza['ID'] ?>">Choose
                </button>
            </div>
            <div class="modal fade" id="exampleModalCenter<?= $pizza['ID'] ?>" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel"><?= $pizza['name'] ?></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body d-flex flex-row">
                            <div class="col-6">
                                <img src="assets/img/pizza.png" style="width: 100%" alt="pizza">
                            </div>

                            <div class="col-6" id="form">
                                <form name="order" method="post" action="components/CartAdd.php">
                                    <div style="overflow-y: scroll; height: 200px">
                                        <?php foreach ($extras as $extraInfo): ?>
                                            <div class="ml-3">
                                                <input id="<?= $extraInfo['ID'] ?>"
                                                       name="extras[]"
                                                       value="<?= $extraInfo['ID'] ?>"
                                                       type="checkbox"">
                                                <label for="<?= $extraInfo['ID'] ?>"><?= $extraInfo['name'] ?></label>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                    <input name="pizza" type="hidden" value="<?= $pizza['ID'] ?>">
                                    <hr>
                                    <button type="submit" class="btn btn-primary">To cart</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.3/dist/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>

</body>
</html>


