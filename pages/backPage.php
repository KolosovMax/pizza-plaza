<?php
require_once 'src/Controller/BackendController.php';

$back = new BackendController();
$ordersInfo = $back->getOrdersInfo();


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.css">
    <title>Orders</title>
</head>
<body>
<?php if ((!isset($_SESSION['isAuth'])) or empty($_SESSION['isAuth']) or !($_SESSION['isAuth'])): ?>
    <div class="container d-flex flex-column justify-content-center" style="min-height: 50vh;max-width: 500px">
        <form method="post" name="authorization" action="components/Auth.php">
            <fieldset>
                <legend class="text-center">Authorization</legend>

                <div>

                    <input class="form-control"
                           type="password"
                           id="password"
                           name="password"
                           placeholder="Enter password">
                </div>

                <div class="text-center">
                    <?php if (!empty($_SESSION['authResponse'])): ?><p><?= $_SESSION['authResponse'] ?></p>

                    <?php endif; ?>
                </div>

                <div class="text-center">
                    <button class="btn-primary btn mt-3" type="submit">Authorization</button>
                </div>
            </fieldset>
        </form>
    </div>
<?php else: ?>

    <div>
        <div class="d-flex">
            <div class="col-2">
                <p>
                    <?= ($_SESSION['employeeInfo']['name'][0]) ?>
                </p>
            </div>
            <div class="col-2">
                <a class="btn btn-primary"
                   href="components/Logout.php"
                   style="width: 5rem">
                    Logout
                </a>
            </div>

        </div>
        <?php foreach ($ordersInfo as $key => $orderInfo): ?>
            <?php $totalPrice[$key] = 0; ?>
            <div>
                <form name="deleteOrder" method="post" action="components/AdminDeleteOrder.php">
                    <input name="id" type="hidden" value="<?= $orderInfo['customer']['ID'] ?>">
                    <h3><?= $orderInfo['customer']['firstname'], $orderInfo['customer']['lastname'] ?></h3>
                    <p><?= $orderInfo['timestamp'] ?></p>
                    <h4>Order:</h4>

                    <?php foreach ($orderInfo['pizzaInfo'] as $pizzaInfo): ?>

                        <div class="d-flex flex-row "><h5 class="mr-3"><?= $pizzaInfo['pizza']['name'] ?></h5>

                            <p class="font-weight-bold">x<?= $pizzaInfo['quantity'] ?></p></div>

                        <?php if ($pizzaInfo['pizza']['ID'] == $_SESSION['sale']): ?>
                            <?php $totalPrice[$key] += round(($pizzaInfo['pizza']['price']) * 0.67, 1) * $pizzaInfo['quantity'] ?>
                        <?php else: ?>
                            <?php $totalPrice[$key] += $pizzaInfo['pizza']['price'] * $pizzaInfo['quantity'] ?>
                        <?php endif; ?>
                        <?php foreach ($pizzaInfo['pizza']['extras'] as $extra): ?>
                            <p><?= $extra['name'] ?></p>
                        <?php endforeach; ?>
                        <?php if (isset($pizzaInfo['additionalExtra'])): ?>
                            <p>Additional Extras:
                                <?php foreach ($pizzaInfo['additionalExtra'] as $extra): ?>
                                    <?= $extra['name'] ?>
                                    <?php $totalPrice[$key] += $extra['price'] ?>
                                <?php endforeach; ?>
                            </p>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <p>Total Price: <?= $totalPrice[$key] ?></p>
                    <button class="btn btn-primary" type="submit">Delete Order</button>
                </form>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
</body>
</html>
