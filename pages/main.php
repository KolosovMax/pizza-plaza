<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '\components\Article.php';
$article = new Article();
$pizzaSale = $article->getPizzaInfoByID($_SESSION['sale']);

?>

<div class="d-flex flex-row mb-5">
    <img class="col-2" src="assets/img/pizza.png" alt="pizza">
    <div class="col-2 d-flex flex-column">
        <h5 class="pl-4"><?= $pizzaSale['name'] ?></h5>
        <p class="pl-4"><?php foreach ($pizzaSale['extras'] as $extra): ?>
                <?= $extra['name'] ?>
            <?php endforeach; ?> </p>
        <h5  class="pl-4" style="color: red;">SALE 30% </h5>
        <a class="btn btn-primary" href="?site=onlineOrder">Order Now</a>
    </div>
</div>

<p class="lead">Herzlich Willkommen auf der Webseite von Pizza Plaza!</p>
<p>Wir sind ein fiktives Restaurant in Gießen/Langgöns. Auf unserer Webseite erfahren Sie nähere Informationen über uns,
    Wege, uns zu kontaktieren und erhalten bald die Möglichkeit, Bestellungen online durchzuführen.</p>
<p>Ihr Restaurant Pizza Plaza</p>