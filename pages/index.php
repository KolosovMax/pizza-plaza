<?php

session_start();

if (isset($_SESSION['cart'])) {
    include_once  $_SERVER['DOCUMENT_ROOT'].'\components\CartPrice.php';
} else {
    $count = 0;
    $price = 0;
}
require_once $_SERVER['DOCUMENT_ROOT'] . '\components\Sale.php';

$sale = new Sale();
$sale->sale();

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pizza Plaza</title>
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/main.css">
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="?site=main">Pizza Plaza</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item <?php if ($currentSite === 'main') echo 'active'; ?>">
                <a class="nav-link" href="?site=main">Startseite</a>
            </li>
            <li class="nav-item <?php if ($currentSite === 'about') echo 'active'; ?>">
                <a class="nav-link" href="?site=about">&Uuml;ber uns</a>
            </li>
            <li class="nav-item <?php if ($currentSite === 'contact') echo 'active'; ?>">
                <a class="nav-link" href="?site=contact">Kontakt</a>
            </li>
            <li class="nav-item <?php if ($currentSite === 'imprint') echo 'active'; ?>">
                <a class="nav-link" href="?site=imprint">Impressum</a>
            </li>
            <li class="nav-item <?php if ($currentSite === 'onlineOrder') echo 'active'; ?>">
                <a class="nav-link" href="?site=onlineOrder">Online ordering</a>
            </li>
            <li class="nav-item <?php if ($currentSite === 'BackPage') echo 'active'; ?>">
                <a class="nav-link" href="?site=BackPage">BackPage</a>
            </li>

        </ul>
    </div>
    <div>
        <a class="btn btn-primary" role="button" href="?site=cart"><?= "$count | $price &#8364" ?><a>
    </div>
</nav>

<div aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="?site=main">Pizza Plaza</a></li>
        <li class="breadcrumb-item active" aria-current="page"><?= $currentSite ?></li>
    </ol>
</div>

<div class="container">
    <?php include $currentSite . '.php'; ?>
</div>

<script src="node_modules/jquery/dist/jquery.min.js"></script>
<script src="node_modules/popper.js/dist/popper.min.js"></script>
<script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="assets/js/main.js"></script>
</body>
</html>
