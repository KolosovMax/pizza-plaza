<?php
include_once 'components/Article.php';

$article = new Article();


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.css">
    <title>Cart</title>
</head>
<body>
<?php if (isset($_SESSION['response'])): ?>
    <div class="text-center text-info ">
        <h3><?= $_SESSION['response'] ?></h3>
    </div>

<?php endif; ?>
<form name="order" method="post" action="components/Order.php">
    <div>
        <?php if (isset($_SESSION['cart']) && (!empty($_SESSION['cart']))): ?>
        <?php foreach ($_SESSION['cart'] as $key => $cart): ?>
            <?php $pizzaInfo = $article->getPizzaInfoByID($cart['pizza']) ?>
            <input name="pizzas[<?= $key ?>][id]" value="<?= $cart['pizza'] ?>" type="hidden">
            <div class="d-flex flex-row mb-5">
                <div class="col-2">
                    <img style="width: 100%" src="assets/img/pizza.png" alt="pizza">
                </div>

                <div class="col-10 d-flex flex-column">
                    <h5 class=""><?= $pizzaInfo['name'] ?></h5>
                    <p class=""><?php foreach ($pizzaInfo['extras'] as $extra): ?>
                            <?= $extra['name'] ?>
                        <?php endforeach; ?>
                    </p>
                    <div class="row align-items-end ">
                        <div class="col-2">
                            <label for="quanity<?= $key ?>">Quantity:</label>
                            <input id="quanity<?= $key ?>"
                                   class="form-control"
                                   name="pizzas[<?= $key ?>][quantity]"
                                   value="<?= $cart['quantity'] ?>"
                                   type="number"
                                   style="width: 5rem;">
                        </div>
                        <div class="col-2">
                            <a class="btn btn-primary"
                               href="components/DeleteOrder.php?key=<?= $key ?>"
                               style="width: 5rem;">
                                Delete
                            </a>
                        </div>
                    </div>
                    <p> Extras:
                        <?php foreach ($cart['extras'] as $extra): ?>
                            <input name="pizzas[<?= $key ?>][extras][]" value="<?= $extra ?>" type="hidden">
                            <?= $article->getExtraName($extra) ?>
                        <?php endforeach; ?>
                        <?php if (empty($cart['extras'])): ?>
                            No extras
                        <?php endif; ?>
                    </p>
                    <?php if ($pizzaInfo['ID'] == $_SESSION['sale']): ?>
                        <h5 style="color: #0d9c0d">Price: <?= round(($pizzaInfo['price']) * 0.67, 1) ?> &#8364!
                            Sale</h5>
                    <?php else: ?>
                        <h6>Price: <?= $pizzaInfo['price'] ?> &#8364</h6>
                    <?php endif; ?>
                </div>

            </div>
        <?php endforeach; ?>

    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="firstName">First name</label>
            <input type="text" class="form-control" name="customer[firstName]" id="firstName" placeholder="First name"
                   required>
        </div>
        <div class="form-group col-md-6">
            <label for="lastName">Last name</label>
            <input type="text" class="form-control" name="customer[lastName]" id="lastName" placeholder="Last name"
                   required>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-4">
            <label for="inputStreet">Street</label>
            <input type="text" class="form-control" name="customer[street]" id="inputStreet" placeholder="Main St">
        </div>
        <div class="form-group col-md-2">
            <label for="inputStreetNumber">Street Number</label>
            <input type="text" class="form-control" name="customer[streetNumber]" id="inputStreetNumber"
                   placeholder="1234">
        </div>
        <div class="form-group col-md-2">
            <label for="inputZip">Zip</label>
            <input type="text" class="form-control" name="customer[zip]" id="inputZip" placeholder="35428">
        </div>
        <div class="form-group col-md-4">
            <label for="inputCity">City</label>
            <input type="text" class="form-control" name="customer[city]" id="inputCity" placeholder="Langgöns">
        </div>
    </div>
    <div class="form-group">
        <label for="phone">Phone</label>
        <input type="text" class="form-control" name="customer[phone]" id="phone" placeholder="+490000000000">
    </div>

    <button type="submit" class="btn btn-primary">Make order</button>
    <?php elseif (!isset($_SESSION['response'])): ?>
        <h3 class="text-center">Cart empty</h3>
    <?php endif; ?>
    <?php unset($_SESSION['response']) ?>
</form>
</body>
</html>
