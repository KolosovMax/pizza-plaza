<?php

interface Repo
{
    public function getAll();
    public function getById(int $id);
    public function save($data): int;
}