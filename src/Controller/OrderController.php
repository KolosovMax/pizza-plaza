<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '\src\db\CustomerRepo.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '\src\db\OrderRepo.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '\src\db\OrderItemsRepo.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '\src\db\OrderItemHasExtraRepo.php';

class OrderController
{


    private $customerRepo;
    private $orderRepo;
    private $orderItemsRepo;
    private $orderItemHasExtraRepo;

    public function __construct()
    {
        $this->customerRepo = new CustomerRepo();
        $this->orderRepo = new OrderRepo();
        $this->orderItemsRepo = new OrderItemsRepo();
        $this->orderItemHasExtraRepo = new OrderItemHasExtraRepo();
    }

    public function makeOrder(): string
    {
        session_start();
        // check customer
        $isCustomerValid = $this->validateCustomer($_POST['customer']);

        if (!$isCustomerValid) {
            return "message about invalid data";
        } else {
            // save customer
            $customerId = $this->customerRepo->save($_POST['customer']);
        }
        // save order
        if (!$customerId) {
            return "message about invalid function";
        } else {
            $orderId = $this->orderRepo->save($customerId);
        }
        if (!$orderId) {
            return "message about invalid function";
        } else {

            foreach ($_POST['pizzas'] as $orderItem) {

                $data = [
                    'orderId' => $orderId,
                    'pizzaId' => $orderItem['id'],
                    'quantity' => $orderItem['quantity']
                ];
                $orderItemId = $this->orderItemsRepo->save($data);
                if (isset($orderItem['extras'])) {

                    foreach ($orderItem['extras'] as $extra) {

                        $extraData = [
                            'orderItemId' => $orderItemId,
                            'extraId' => $extra
                        ];
                        $this->orderItemHasExtraRepo->save($extraData);
                    }
                }

            }
            unset($_SESSION['cart']);
            return 'Order confirmed';
        }
    }

    public function getAllOrders(): array
    {
        return $this->orderRepo->getAll();
    }

    public function getCustomerById($id): array
    {
        return $this->customerRepo->getById($id);
    }

    public function getOrderItems(): array
    {
        return $this->orderItemsRepo->getAll();
    }

    public function getOrderItemsByOrderId($id): array
    {
        return $this->orderItemsRepo->getByOrderId($id);
    }

    public function getOrderHasExtraInfoById($id): array
    {
        return $this->orderItemHasExtraRepo->getByOrderItemId($id);
    }

    private function validateCustomer($customerData): bool
    {
        if (isset($customerData['firstName']) && isset($customerData['lastName'])) return true;
        else return false;
        /* $phoneNumberRegExp = "/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/";

        if (!preg_match($phoneNumberRegExp, $customerData["phone"])) return false;

         return true;*/
    }

    public function deleteCustomerInfo($id): void
    {
        $this->customerRepo->deleteById($id);
    }
}