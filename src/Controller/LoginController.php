<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '\src\db\AdminUserRepo.php';

class LoginController
{
    private $adminUserRepo;

    public function __construct()
    {
        $this->adminUserRepo = new AdminUserRepo();
    }

    public function authValidate($pass): void
    {
        $users = $this->adminUserRepo->getAll();
        foreach ($users as $user) {
            if ($pass == $user['password']) {
                $_SESSION['employeeInfo']['name'][] = $user['login'];
                $_SESSION['employeeInfo']['isAdmin'][] = $user['isAdmin'];
                $_SESSION['isAuth'][] = true;
                break;
            } else $_SESSION['isAuth'] = false;
        }
    }
}
