<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '\components\Article.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '\src\Controller\OrderController.php';

class BackendController
{
    public $article;
    public $orderCntrl;

    public function __construct()
    {
        $this->article = new Article();
        $this->orderCntrl = new OrderController();
    }

    public function getOrdersInfo(): array
    {
        $orderInfo = $this->orderCntrl->getAllOrders();
        $orderInfo = $this->addCustomerInfo($orderInfo);
        $orderInfo = $this->addPizzasInfo($orderInfo);

        return $orderInfo;

    }

    private function addCustomerInfo($data): array
    {
        foreach ($data as &$order) {
            $customer = $this->orderCntrl->getCustomerById($order['customer_ID']);
            $order['customer'] = $customer;
        }
        return $data;
    }

    private function addPizzasInfo($data): array
    {
        foreach ($data as &$order) {

            $items = $this->orderCntrl->getOrderItemsByOrderId($order['ID']);


            foreach ($items as $key => &$item) {
                $pizzaInfo = $this->article->getPizzaInfoByID($item['Pizzas_ID']);
                $order['pizzaInfo'][$key]['pizza'] = $pizzaInfo;

                $extras = $this->orderCntrl->getOrderHasExtraInfoById($item['ID']);

                if (!empty ($extras)) {
                    foreach ($extras as $extra) {
                        $extraInfo = $this->article->getExtraInfoById($extra['Extras_ID']);
                        $order['pizzaInfo'][$key]['additionalExtra'][] = $extraInfo;
                    }
                }
                $order['pizzaInfo'][$key]['quantity'] = $item['quantity'];

            }

        }

        return $data;
    }

    public function deleteInfo($id): void
    {
        $this->orderCntrl->deleteCustomerInfo($id);
    }
}