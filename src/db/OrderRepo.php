<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '\src\kernel\repo.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '\src\kernel\db-connection.php';

class OrderRepo implements Repo
{
    private $dbConnection;

    public function __construct()
    {

        $this->dbConnection = DbConnection::getInstance()->getConnection();
    }

    public function getAll()
    {
        $sql = "SELECT * FROM `Order`";

        $statement = $this->dbConnection->prepare($sql);
        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getById(int $id)
    {
        // TODO: Implement getById() method.
    }

    public function save($id): int
    {
        // TODO: Implement save() method.
        $dataValue = [
            'timestamp' => date('c'),
            'customer_id' => $id,
        ];

        $sql = "INSERT INTO `Order`(timestamp, customer_ID)  
                VALUES (:timestamp, :customer_id) ";

        $statement = $this->dbConnection->prepare($sql);
        $statement->execute($dataValue);

        return $this->dbConnection->lastInsertId();
    }

}