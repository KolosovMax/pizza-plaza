<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '\src\kernel\repo.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '\src\kernel\db-connection.php';

class AdminUserRepo implements Repo
{
    private $dbConnection;

    public function __construct()
    {

        $this->dbConnection = DbConnection::getInstance()->getConnection();
    }

    public function getAll(): array
    {
        $sql = "SELECT * FROM Admin_user";

        $statement = $this->dbConnection->prepare($sql);
        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getById(int $id)
    {
        // TODO: Implement getById() method.
    }

    public function save($data): int
    {
        // TODO: Implement save() method.
    }
}