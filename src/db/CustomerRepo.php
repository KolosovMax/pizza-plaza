<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '\src\kernel\repo.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '\src\kernel\db-connection.php';

class CustomerRepo implements Repo
{
    private $dbConnection;

    public function __construct()
    {

        $this->dbConnection = DbConnection::getInstance()->getConnection();
    }

    public function getAll()
    {
        // TODO: Implement getAll() method.
    }

    public function getById(int $id)
    {
        $sql = "SELECT * FROM Customer WHERE ID = :id";

        $statement = $this->dbConnection->prepare($sql);
        $statement->execute(['id' => $id]);

        return $statement->fetch(PDO::FETCH_ASSOC);
    }

    public function save($data): int
    {
        // TODO: Implement save() method.
        $dataValue = [
            'firstname' => $data['firstName'],
            'lastname' => $data['lastName'],
            'street' => $data['street'],
            'streetnumber' => $data['streetNumber'],
            'zip' => $data['zip'],
            'city' => $data['city'],
            'phone' => $data['phone'],
        ];

        $sql = "INSERT INTO Customer(firstname, lastname, street, streetnumber, zip, city, phone)  
                VALUES (:firstname, :lastname, :street, :streetnumber, :zip, :city, :phone) ";

        $statement = $this->dbConnection->prepare($sql);
        $statement->execute($dataValue);

        return $this->dbConnection->lastInsertId();

    }

    public function deleteById($id): void
    {
        $sql = "DELETE  FROM Customer WHERE id = :id";

        $statement = $this->dbConnection->prepare($sql);
        $statement->execute(['id' => $id]);
    }
}