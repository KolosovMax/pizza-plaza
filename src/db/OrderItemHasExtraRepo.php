<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '\src\kernel\repo.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '\src\kernel\db-connection.php';

class OrderItemHasExtraRepo implements Repo
{
    private $dbConnection;

    public function __construct()
    {

        $this->dbConnection = DbConnection::getInstance()->getConnection();
    }

    public function getAll()
    {
        // TODO: Implement getAll() method.
    }

    public function getById(int $id)
    {
        $sql = "SELECT * FROM Orderitem_has_Extra WHERE ID = :id";

        $statement = $this->dbConnection->prepare($sql);
        $statement->execute(['id' => $id]);

        return $statement->fetch(PDO::FETCH_ASSOC);
    }

    public function save($data): int
    {
        $dataValue = [
            'orderItemId' => $data['orderItemId'],
            'extraId' => $data['extraId'],
        ];


        $sql = "INSERT INTO Orderitem_has_Extra(Orderitems_ID, Extras_ID)  
                VALUES (:orderItemId, :extraId) ";

        $statement = $this->dbConnection->prepare($sql);
        $statement->execute($dataValue);

        return $this->dbConnection->lastInsertId();
    }

    public function getByOrderItemId(int $id)
    {
        $sql = "SELECT * FROM Orderitem_has_Extra WHERE Orderitems_ID = :id";

        $statement = $this->dbConnection->prepare($sql);
        $statement->execute(['id' => $id]);

        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }
}