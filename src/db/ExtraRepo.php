<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '\src\kernel\repo.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '\src\kernel\db-connection.php';

class ExtraRepo implements Repo
{

    private $dbConnection;

    public function __construct()
    {

        $this->dbConnection = DbConnection::getInstance()->getConnection();
    }


    public function getAll()
    {
        $sql = "SELECT * FROM Extras";

        $statement = $this->dbConnection->prepare($sql);
        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getById(int $id)
    {
        $sql = "SELECT * FROM Extras WHERE ID=:id";

        $statement = $this->dbConnection->prepare($sql);
        $statement->execute(['id' => $id]);

        return $statement->fetch(PDO::FETCH_ASSOC);
    }

    public function getByPizzaId(int $id)
    {
        $sql = "SELECT  e.id, e.name, e.price, e.isChoosable FROM Extras e JOIN Pizza_has_Extra pe on e.id = pe.Extras_ID WHERE pe.Pizzas_ID = :pizza_id";

        $statement = $this->dbConnection->prepare($sql);
        $statement->execute(['pizza_id' => $id]);

        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function save($data): int
    {
        // TODO: Implement save() method.
    }

    public function getAllChoosable()
    {
        $sql = "SELECT * FROM Extras WHERE isChoosable=1";

        $statement = $this->dbConnection->prepare($sql);
        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }
}