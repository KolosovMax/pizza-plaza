<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '\src\kernel\repo.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '\src\kernel\db-connection.php';

class OrderItemsRepo implements Repo
{
    private $dbConnection;

    public function __construct()
    {
        $this->dbConnection = DbConnection::getInstance()->getConnection();
    }

    public function getAll()
    {
        $sql = "SELECT * FROM OrderItems";

        $statement = $this->dbConnection->prepare($sql);
        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getById(int $id)
    {
        $sql = "SELECT * FROM OrderItems WHERE ID = :id";

        $statement = $this->dbConnection->prepare($sql);
        $statement->execute(['id' => $id]);

        return $statement->fetch(PDO::FETCH_ASSOC);
    }

    public function save($data): int
    {
        $dataValue = [
            'orderId' => $data['orderId'],
            'pizzaId' => $data['pizzaId'],
            'quantity' => $data['quantity']
        ];

        $sql = "INSERT INTO Orderitems(quantity,order_ID, pizzas_ID)  
                VALUES (:quantity, :orderId, :pizzaId) ";

        $statement = $this->dbConnection->prepare($sql);
        $statement->execute($dataValue);

        return $this->dbConnection->lastInsertId();
    }

    public function getByOrderId(int $id)
    {
        $sql = "SELECT * FROM OrderItems WHERE Order_ID = :id";

        $statement = $this->dbConnection->prepare($sql);
        $statement->execute(['id' => $id]);

        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

}