<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '\src\kernel\Repo.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '\src\kernel\db-connection.php';

class PizzaRepo implements Repo
{

    private $dbConnection;

    public function __construct()
    {

        $this->dbConnection = DbConnection::getInstance()->getConnection();
    }

    public function getAll()
    {
        $sql = "SELECT * FROM Pizzas";

        $statement = $this->dbConnection->prepare($sql);
        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getById(int $id)
    {
        $sql = "SELECT * FROM Pizzas WHERE ID=:id";

        $statement = $this->dbConnection->prepare($sql);
        $statement->execute(['id' => $id]);

        return $statement->fetch(PDO::FETCH_ASSOC);

        // TODO: Implement getById() method.
    }

    public function save($data): int
    {
        // TODO: Implement save() method.
    }

    public function getId()
    {

        $sql = "SELECT ID FROM Pizzas";

        $statement = $this->dbConnection->prepare($sql);
        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }
}
