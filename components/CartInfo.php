<?php
require_once 'components/Article.php';

class CartInfo
{
    private $Article;

    public function __construct()
    {
        $this->Article = new Article();
    }

    public function getPizzasPrice(): float
    {
        $pizzasPrice = 0;

        foreach ($_SESSION['cart'] as $cart) {

            if ($cart['pizza'] == $_SESSION['sale']) {

                $pizzasPrice += round((($this->Article->getPizzaPrice($cart['pizza'])) * 0.67 * $cart['quantity']), 1);
            } else {

                $pizzasPrice += ($this->Article->getPizzaPrice($cart['pizza'])) * $cart['quantity'];
            }
        }
        return $pizzasPrice;
    }

    public function getExtrasPrice(): float
    {
        $extrasPrice = 0;

        foreach ($_SESSION['cart'] as $cart) {
            if (is_array($cart['extras'])) {
                foreach ($cart['extras'] as $extra) {
                    $extrasPrice += ($this->Article->getExtraPrice($extra)) * $cart['quantity'];
                }
            }
        }
        return $extrasPrice;
    }

    public function getPizzasCount(): int
    {
        return count($_SESSION['cart']);
    }
}