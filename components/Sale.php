<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '\components\Article.php';

class Sale
{
    public $article;


    public function __construct()
    {
        $this->article = new Article();

    }

    public function sale(): void
    {
        $fileName = 'saleInfo';
        if ($this->saleInfoValidate($fileName)) {
            $saleInfo = $this->saleInfoValidate($fileName);
        } else {
            $saleInfo = $this->saveToFile($fileName);
        }

        $_SESSION['sale'] = $saleInfo;

    }

    private function saleInfoValidate($fileName): string
    {
        if (file_exists($fileName)) {
            $saleInfo = explode(':', file_get_contents($fileName));

            if ((time() - $saleInfo[1]) > 86400) {

                return $this->saveToFile($fileName);
            } else return $saleInfo[0];

        } else {
            return false;
        }
    }

    private function saveToFile($fileName): string
    {
        $saleInfo = $this->saleInfo();
        file_put_contents($fileName, $saleInfo);
        $saleId = explode(':', file_get_contents($fileName))[0];
        return $saleId;
    }

    private function saleInfo(): string
    {
        $pizzaId = $this->article->getRandomId();
        return $pizzaId . ':' . time();

    }
}