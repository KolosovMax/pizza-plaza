<?php
session_start();
//Because of the structure of the site, I did part of the task with code in one file.
//and the other part using classes and functions

if (isset($_POST['extras'])) { // checking extras in order
    if (isset($_SESSION['cart'])) {
        foreach ($_SESSION['cart'] as &$cart) {
            if ($cart['pizza'] === $_POST['pizza']) { // checking pizza's id
                if ($_POST['extras'] == $cart['extras']) {
                    $in_cart = true;  // if extra's array are equal, set $in_cart, increasing the number of pizza, break the
                    $cart['quantity'] += 1;
                    break;
                } else {
                    $in_cart = false;//  if extra's array are not equal,  checking nex pizza
                }
            } else {
                $in_cart = false; //if the is no pizza with id, $in_cart false;
            }
        }
        if (!$in_cart) {
            $_SESSION['cart'][] = array('pizza' => $_POST['pizza'], 'extras' => $_POST['extras'], 'quantity' => 1);
        } // adding pizza after checking in_cart variable

    } else {
        $_SESSION['cart'][] = array('pizza' => $_POST['pizza'], 'extras' => $_POST['extras'], 'quantity' => 1);//if cart empty, adding order
    }
} else { //if no extra in order -set variable equal 0
    $_POST['extras'] = [];
    if (isset($_SESSION['cart'])) { //if cart not empty , compare pizza's
        foreach ($_SESSION['cart'] as &$cart) {
            if ($cart['pizza'] === $_POST['pizza']) {
                $in_cart = true;
                $cart['quantity'] += 1;
                break;
            } else {
                $in_cart = false;
            }
        }
        if (!$in_cart) {
            $_SESSION['cart'][] = array('pizza' => $_POST['pizza'], 'extras' => $_POST['extras'], 'quantity' => 1);
        }//adding order if in_cart= false after all cycle
    } else {
        $_SESSION['cart'][] = array('pizza' => $_POST['pizza'], 'extras' => $_POST['extras'], 'quantity' => 1);// if cart empty, adding order
    }
}

header('Location: ../?site=onlineOrder');