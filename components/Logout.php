<?php
session_start();

unset($_SESSION['isAuth']);
unset($_SESSION['employeeInfo']);

header('Location: ../$site=main');