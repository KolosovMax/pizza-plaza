<?php
require_once $_SERVER['DOCUMENT_ROOT'] .'\src\db\ExtraRepo.php';
require_once $_SERVER['DOCUMENT_ROOT'] .'\src\db\PizzaRepo.php';

class Article
{
    private $pizzaRepo;
    private $extraRepo;

    public function __construct()
    {
        $this->pizzaRepo = new PizzaRepo();
        $this->extraRepo = new ExtraRepo();
    }

    public function getPizzaInfo(): array
    {
        $pizzas = $this->pizzaRepo->getAll();

        foreach ($pizzas as &$pizza) {
            $extras = $this->extraRepo->getByPizzaId($pizza['ID']);
            $pizza['extras'] = $extras;
        }
        return $pizzas;
    }

    public function getPizzaPrice($id): float
    {
        $pizza = $this->pizzaRepo->getById($id);

        return $pizza['price'];
    }

    public function getAllExtras(): array
    {
        return $this->extraRepo->getAll();
    }
    public function getAllChoosableExtras(): array
    {
        return $this->extraRepo->getAllChoosable();
    }

    public function getExtraPrice($id): float
    {
        $extra = $this->extraRepo->getById($id);

        return $extra['price'];
    }

    public function getExtraName($id): string
    {
        $extra = $this->extraRepo->getById($id);

        return $extra['name'];
    }

    public function getExtraInfoById($id): array
    {
        $extra = $this->extraRepo->getById($id);

        return $extra;
    }

    public function getRandomId(): int
    {
        $pizzas = $this->pizzaRepo->getId();
        foreach ($pizzas as $pizza) {
            $id[] = (current(($pizza)));
        }
        $key_id = array_rand($id, 1);
        $id = $id[$key_id];

        return $id;
    }


    public function getPizzaInfoByID($id): array
    {
        $pizza = $this->pizzaRepo->getById($id);

        $extras = $this->extraRepo->getByPizzaId($id);
        $pizza['extras'] = $extras;

        return $pizza;
    }
}
