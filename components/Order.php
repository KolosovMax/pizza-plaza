<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '\src\Controller\OrderController.php';

$orderCntrl = new OrderController();

$response = $orderCntrl->makeOrder();
session_start();
$_SESSION['response'] = $response;
header('Location: ../?site=cart');