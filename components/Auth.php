<?php
require_once  $_SERVER['DOCUMENT_ROOT'] . '\src\Controller\LoginController.php';
session_start();
$login = new LoginController();

$login->authValidate($_POST['password']);

if ($_SESSION['isAuth']){
    header('Location: ../?site=BackPage');
}else{
    $_SESSION['authResponse'] = "Incorrect password";

    header('Location: ../?site=BackPage');
}