<?php
include_once 'components/CartInfo.php';

$pizzaInfo = new CartInfo();
$pizzaPrice = $pizzaInfo->getPizzasPrice();
$extrasPrice= $pizzaInfo->getExtrasPrice();
$price= $pizzaPrice + $extrasPrice;
$count = $pizzaInfo->getPizzasCount();
